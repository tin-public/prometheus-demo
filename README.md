# Prometheus-demo
## Requirement
- vagrant
- ansible

## How to run
### Create VMs
```
vagrant up
```
### Change sensitive data
There are some sensitive data encrypted by ansible-vault. You have to change them before running ansible-playbook.  
  
File `group_vars/grafana/vault`
```
vault_grafana_password: <<YOUR_GRAFANA_PASSWORD>>
```
  
File `group_vars/alertmanager/vault`
```
vault_alertmanager_slack_api_url: <<YOUR_SLACK_API_URL>>
```

You also have to remove `vault_password_file=.vault-password` option in `ansible.cfg`.

### Install ansible roles
```
ansible-galaxy install -r requirements.yml -p roles
```

### Run ansible-playbook
```
ansible-playbook provision.yml
```
> In Mac, if `ERROR! A worker was found in a dead state` occurs. Run `OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES ansible-playbook provision.yml` instead

After ansible-playbook run successfully. You can access
- Prometheus UI at http://10.3.3.10:9090.
- Grafana dashboard at http://10.3.3.10:3000. Login with `admin` username and your grafana password.
### Check alerting
There is a default alert (InstanceDown). Try stopping `n1` or `n2` vm by running `vagrant halt n2`. An alert should be sent to your slack channel.
